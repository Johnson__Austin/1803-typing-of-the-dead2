﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AJWordSpawner : MonoBehaviour {

    public GameObject wordPrefab;
    public Transform wordCanvas;

	public AJWordDisplay SpawnWord()
    {
        GameObject wordObj = Instantiate(wordPrefab, wordCanvas);
        AJWordDisplay wordDisplay = wordObj.GetComponent<AJWordDisplay>();

        return wordDisplay;
    } 
}

//        Developer Name: Austin Johnson

//         Contribution: Typing of the Dead 2

//                Feature: Word Bank & Text Boxes

//                Start & End dates: March 11, 2018 - March 14, 2018

//                References: "How to make a Typing Game in Unity (Livestream)." Youtube, uploaded by Bracksys,
//                          11 March 2018, www.youtube.com/watch?v=HvMrOoUeqO0&t=305s

//                        Links: https://www.youtube.com/watch?v=HvMrOoUeqO0&t=305s
