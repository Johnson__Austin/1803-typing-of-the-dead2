﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AJWordGenerator : MonoBehaviour {

    private static string[] wordList = { "chair", "ladder", "jumping", "crane", "phantasm", "kangaroo", "chimpanzee", "trucker" };


    public static string GetWord()
    {
        int wordIndex = Random.Range(0, wordList.Length);
        string randomWord = wordList[wordIndex];

        return randomWord;
    }
}

//        Developer Name: Austin Johnson

//         Contribution: Typing of the Dead 2

//                Feature: Word Bank & Text Boxes

//                Start & End dates: March 11, 2018 - March 14, 2018

//                References: "How to make a Typing Game in Unity (Livestream)." Youtube, uploaded by Bracksys,
//                          11 March 2018, www.youtube.com/watch?v=HvMrOoUeqO0&t=305s

//                        Links: https://www.youtube.com/watch?v=HvMrOoUeqO0&t=305s
