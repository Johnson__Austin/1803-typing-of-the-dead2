﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CF_PlayerHealth : MonoBehaviour {

    public int curHealth;
    public int maxHealth = 1;

	// Use this for initialization
	void Start () {

        curHealth = maxHealth;
		
	}
	
	// Update is called once per frame
	void Update () {

        if(curHealth > maxHealth)
        {
            curHealth = maxHealth;
        }
        if (curHealth <= 0)
        {
            // Death should happen here
        }
		
	}
}
