﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CF_HUD : MonoBehaviour {

    public Sprite[] HealthTorch;
    public Image TorchUI;
    private CF_PlayerHealth Player;

	// Use this for initialization
	void Start () {

        Player = GameObject.FindGameObjectWithTag("Player").GetComponent<CF_PlayerHealth>();
		
	}
	
	// Update is called once per frame
	void Update () {

        TorchUI.sprite = HealthTorch[Player.curHealth];
		
	}
}
