﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CF_HealthPickup : MonoBehaviour {

    CF_PlayerHealth GetPlayerHealth;
    public int addHealth = 1;

    void Awake()
    {
        GetPlayerHealth = FindObjectOfType<CF_PlayerHealth>();
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKeyDown(KeyCode.Return))
        {
            if (GetPlayerHealth.curHealth <= 0)
            {
                Destroy(gameObject);
                GetPlayerHealth.curHealth = GetPlayerHealth.curHealth + addHealth;
            }
        }
            
	}

    
}
