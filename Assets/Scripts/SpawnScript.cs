﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnScript : MonoBehaviour {

    //Cody Pena
    //03.16.18
    //Reference https://www.youtube.com/watch?v=M_xXmpI0GYs
    //Array for spawn locations you set with empty children
    public Transform[] spawnLocations;
    //Prefabs you want to spawn
    public GameObject[] whatPrefabToSpawn;
    //Clones of what prefabs. BE SURE THE ORDER MATCHES ABOVE IN THE INSPECTOR.
    public GameObject[] whatPrefabToSpawnClone;

    //Change on start to on colision to spawn on colision instead of at start.
    //then create volumes player will walk through and trigger a spawn of however many of any units you want. 
    //This will allow you to use this script/oject set up multiple times in different places.
    void Start()
    {
        spawnGameObject();
    }
    
    void spawnGameObject()
    {
        whatPrefabToSpawnClone[0] = Instantiate(whatPrefabToSpawn[0], spawnLocations[0].transform.position, Quaternion.Euler(0, 0, 0)) as GameObject;
        whatPrefabToSpawnClone[1] = Instantiate(whatPrefabToSpawn[1], spawnLocations[1].transform.position, Quaternion.Euler(0, 0, 0)) as GameObject;
        whatPrefabToSpawnClone[2] = Instantiate(whatPrefabToSpawn[2], spawnLocations[2].transform.position, Quaternion.Euler(0, 0, 0)) as GameObject;
        whatPrefabToSpawnClone[3] = Instantiate(whatPrefabToSpawn[3], spawnLocations[3].transform.position, Quaternion.Euler(0, 0, 0)) as GameObject;
        whatPrefabToSpawnClone[4] = Instantiate(whatPrefabToSpawn[4], spawnLocations[4].transform.position, Quaternion.Euler(0, 0, 0)) as GameObject;
    } 
}
