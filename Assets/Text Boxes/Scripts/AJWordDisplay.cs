﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AJWordDisplay : MonoBehaviour {

    public Text text;

    public void SetWord (string word)
    {
        text.text = word;
    }

    public void RemoveLetter()
    {
        text.text = text.text.Remove(0, 1);
        text.color = Color.red;
    }

    public void RemoveWord()
    {
        Destroy(gameObject);
    }

}

//        Developer Name: Austin Johnson

//         Contribution: Typing of the Dead 2

//                Feature: Word Bank & Text Boxes

//                Start & End dates: March 11, 2018 - March 14, 2018

//                References: "How to make a Typing Game in Unity (Livestream)." Youtube, uploaded by Bracksys,
//                          11 March 2018, www.youtube.com/watch?v=HvMrOoUeqO0&t=305s

//                        Links: https://www.youtube.com/watch?v=HvMrOoUeqO0&t=305s
