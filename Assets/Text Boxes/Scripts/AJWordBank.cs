﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AJWordBank : MonoBehaviour {

    public List<AJWord> wordList;

    public AJWordSpawner wordSpawner;

    private bool hasActiveWord;
    private AJWord activeWord;

    private void Start()
    {
        AddWord();
    }

    public void AddWord()
    {
        AJWord Word = new AJWord(AJWordGenerator.GetWord(), wordSpawner.SpawnWord());
        Debug.Log(Word.word);

        wordList.Add(Word);
    }

    public void TypeLetter (char letter)
    {
        if (hasActiveWord)
        {
            if (activeWord.GetNextLetter() == letter)
            {
                activeWord.TypeLetter();
            }
        }
        else
        {
            foreach(AJWord word in wordList)
            {
                if (word.GetNextLetter() == letter)
                {
                    activeWord = word;
                    hasActiveWord = true;
                    word.TypeLetter();
                    break;
                }
            }
        }

        if (hasActiveWord && activeWord.WordTyped())
        {
            hasActiveWord = false;
            wordList.Remove(activeWord);
        }
    }
}

//        Developer Name: Austin Johnson

//         Contribution: Typing of the Dead 2

//                Feature: Word Bank & Text Boxes

//                Start & End dates: March 11, 2018 - March 14, 2018

//                References: "How to make a Typing Game in Unity (Livestream)." Youtube, uploaded by Bracksys,
//                          11 March 2018, www.youtube.com/watch?v=HvMrOoUeqO0&t=305s

//                        Links: https://www.youtube.com/watch?v=HvMrOoUeqO0&t=305s