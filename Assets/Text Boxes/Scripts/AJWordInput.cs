﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AJWordInput : MonoBehaviour {

    public AJWordBank wordManager;
	
	// Update is called once per frame
	void Update () {
        foreach (char letter in Input.inputString)
        {
            wordManager.TypeLetter(letter);
        }
		
	}
}

//        Developer Name: Austin Johnson

//         Contribution: Typing of the Dead 2

//                Feature: Word Bank & Text Boxes

//                Start & End dates: March 11, 2018 - March 14, 2018

//                References: "How to make a Typing Game in Unity (Livestream)." Youtube, uploaded by Bracksys,
//                          11 March 2018, www.youtube.com/watch?v=HvMrOoUeqO0&t=305s

//                        Links: https://www.youtube.com/watch?v=HvMrOoUeqO0&t=305s